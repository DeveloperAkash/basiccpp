#include<iostream>
#include<array>

int main()
{

    //Declare
    std::array<int, 5> myarray1;

    //Initialization
    std::array<int,5> myarray2={1,2,3,4,5};//Initializer List
    std::array<int,5> myarray3{1,2,3,4,5};//uniform Initialization

    //Assign using initializer list
    std::array<int,5> myarray4;
    myarray4={1,2,3,4,5};

    return 0;
}